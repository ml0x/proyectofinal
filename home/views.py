from django.shortcuts import render
from app.models import Banco, TipoCuenta,Usuario, Deuda, Gasto, Categoria
from django.contrib.auth.decorators import login_required

# Create your views here.
#
def web(request):
    template_name = 'web.html'
    data = {}
    return render(request, template_name, data)

@login_required(login_url="/auth/login")
def home(request):
    template_name = 'home.html'
    data = {}
    return render(request, template_name, data)

def pagos(request):
    template_name = 'pagos.html'
    data = {}
    data['gastos'] = Gasto.objects.all()
    return render(request, template_name, data)

def deudas(request):
    template_name = 'deudas.html'
    data = {}
    data['deudas'] = Deuda.objects.all()

    return render(request, template_name, data)

def pagosRecibidos(request):
    template_name = 'pagosRecibidos.html'
    data = {}

    return render(request, template_name, data)

def consolidarDeuda(request):
    template_name = 'consolidarDeuda.html'
    data = {}
    data['deudas'] = Deuda.objects.all()
    data['total'] = 0
    return render(request, template_name, data)

def reporte(request):
    template_name = 'reporte.html'
    data = {}

    return render(request, template_name, data)

def cuenta(request):
    template_name = 'cuenta.html'
    data = {}
    data['tipoCuentas'] = TipoCuenta.objects.all()
    data['bancos'] = Banco.objects.all()
    data['usuarios'] = Usuario.objects.all()

    return render(request, template_name, data)

def agregarPago(request):
    template_name = 'agregarPago.html'
    data = {}
    data['categorias'] = Categoria.objects.all() 
    data['usuarios'] = Usuario.objects.all()
    

    return render(request, template_name, data)

def pagarDeuda(request):
    template_name = 'pagarDeuda.html'
    data = {}
    data['usuarios'] = Usuario.objects.all()

    return render(request, template_name, data)
    