from django.urls import path
from . import views

urlpatterns = [
    path('', views.web, name='web'),
    path('home', views.home, name='home'),
    path('pagos', views.pagos, name="pagos"),
    path('deudas', views.deudas, name="deudas"),
    path('pagosRecibidos', views.pagosRecibidos, name="pagosRecibidos"),
    path('consolidarDeuda', views.consolidarDeuda, name="consolidarDeuda"),
    path('reporte', views.reporte, name="reporte"),
    path('cuenta', views.cuenta, name="cuenta"),
    path('agregarPago', views.agregarPago, name="agregarPago"),
    path('pagarDeuda', views.pagarDeuda, name="pagarDeuda"),
]