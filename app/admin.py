from django.contrib import admin
from app.models import Banco, TipoCuenta, Categoria, Usuario, Deuda, Gasto

@admin.register(Banco)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "banco",
        "fecha_agregado",
        "estado",
    ]

@admin.register(TipoCuenta)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "tipo",
        "fecha_agregado",
        "estado",
    ]

@admin.register(Categoria)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "categoria",
        "fecha_agregado",
        "estado",
    ]

@admin.register(Usuario)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "rut",
        "nombre",
        "apellido",
        "cuenta",
        "email",
        "banco",
        "tipo_cuenta",
        "fecha_agregado",
        "estado",
    ]

@admin.register(Deuda)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "producto",
        "acreedor",
        "monto",
        "fecha_agregado",
        "aceptacion",
        "estado",
    ]

@admin.register(Gasto)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "producto",
        "acreedor",
        "monto",
        "fecha_agregado",
    ]