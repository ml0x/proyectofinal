from django.db import models


class Banco (models.Model):
    banco = models.CharField(max_length=144)
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.banco

class TipoCuenta (models.Model):
    tipo = models.CharField(max_length=144)
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.tipo

class Categoria (models.Model):
    categoria = models.CharField(max_length=144)
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.categoria

class Usuario(models.Model):
    user = models.CharField(max_length=144)
    rut = models.CharField(max_length=11)
    nombre = models.CharField(max_length=144)
    apellido = models.CharField(max_length=144)
    cuenta = models.CharField(max_length=144)
    email = models.EmailField(null=True, blank=True)
    banco = models.ForeignKey(Banco, on_delete=models.CASCADE)
    tipo_cuenta = models.ForeignKey(TipoCuenta, on_delete=models.CASCADE)
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.user

class Deuda(models.Model):
    producto = models.CharField(max_length=144)
    acreedor = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    monto = models.IntegerField()
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    aceptacion = models.BooleanField(default=True)
    estado = models.BooleanField(default=True)

class Gasto(models.Model):
    producto = models.CharField(max_length=144)
    acreedor = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    monto = models.IntegerField()
    fecha_agregado = models.DateTimeField(auto_now_add=True)



